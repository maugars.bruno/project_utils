## Introduction ##
`project_utils` offers some git submodule (alias and commands) and cmake utilities (macros functions and Find.cmake scripts). It also documents the intended workflow to use these utilities.

## Dependencies ##
`project_utils` is a cmake 3.12 library.

## Acknowledgements ##
Some of the Find.cmake scripts have been copy-paste from the internet, then modified. A first-line comment gives the source it was copied from.

## License ##
`project_utils` is available under the MPL-2.0 license (https://mozilla.org/MPL/2.0/).
